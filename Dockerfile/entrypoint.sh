#!/bin/sh

if [ "$DEBUG" = "1" ]; then
  pyroscope exec --spy-name=$PYROSCOPE_SPY_NAME --application-name=$PYROSCOPE_APPLICATION_NAME  --server-address=$PYROSCOPE_SERVER_ADDRESS flask run
elif [ "$DEBUG" = "0" ]; then
  flask run
fi

